package com.alexbg.balonmanocomboboxxml.base;

public class Jugador {
    private String nombre;
    private double peso;
    private int edad;
    private String posicion;

    public Jugador(String nombre, double peso, int edad, String posicion){
        this.nombre = nombre;
        this.peso = peso;
        this.edad = edad;
        this.posicion = posicion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getPosicion() {
        return posicion;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    @Override
    public String toString() {
        return "Jugador{" +
                "nombre='" + nombre + '\'' +
                ", peso=" + peso +
                ", edad=" + edad +
                ", posicion='" + posicion + '\'' +
                '}';
    }
}
