package com.alexbg.balonmanocomboboxxml.base;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

public class Vista implements ActionListener {
    private JFrame frame;
    private JPanel panel1;

    private JTextField nombreTxt;
    private JTextField pesoTxt;
    private JTextField edadTxt;

    private JComboBox comboBox;
    private JComboBox combobox1;

    private JButton altaJugadorButton;
    private JButton bajaJugadorButton;
    private JButton modificarJugadorButton;
    private JButton mostrarDatosButton;
    private JLabel lblInfo;

    private JMenuItem itemExportarXML;
    private JMenuItem itemImportarXML;

    //Elementos añadidos por mi
    private LinkedList<Jugador> lista;
    private DefaultComboBoxModel<Jugador> dcbm;
    private DefaultComboBoxModel<Vista> dcbmInfo;


    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        lista = new LinkedList<>();
        dcbm = new DefaultComboBoxModel<>();
        dcbmInfo = new DefaultComboBoxModel<>();
        comboBox.setModel(dcbm);
        combobox1.setModel(dcbmInfo);

        crearMenu();
        aniadirDatosComboBox();

        altaJugadorButton.addActionListener(this);
        //bajaJugadorButton.addActionListener(this);
        mostrarDatosButton.addActionListener(this);
        itemExportarXML.addActionListener(this);
        itemImportarXML.addActionListener(this);
    }

    public static void main (String[] args){
        Vista vista = new Vista();

    }

    private void altaJugador(){
        String nombre = nombreTxt.getText();
        double peso = Double.parseDouble(pesoTxt.getText());
        int edad = Integer.parseInt(edadTxt.getText());
        String posicion = (String) combobox1.getSelectedItem();

        lista.add(new Jugador(nombre, peso, edad, posicion));
        refrescarComboBox();
    }


    private void mostrarDatosLinkedList(){
        for (Jugador jugador: lista) {
            System.out.println(jugador);
        }
    }

    private void aniadirDatosComboBox(){
        comboBox.addItem("DATOS");
        combobox1.addItem("Portero");
        combobox1.addItem("Pivote");
        combobox1.addItem("Central");
        combobox1.addItem("Lateral");
        combobox1.addItem("Extremo");
    }

    private void refrescarComboBox() {
        dcbm.removeAllElements();
        for (Jugador jugador : lista) {
            dcbm.addElement(jugador);
        }
    }

    private void crearMenu(){
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemExportarXML = new JMenuItem("Exportar XML");
        itemImportarXML = new JMenuItem("Importar XML");

        menu.add(itemExportarXML);
        menu.add(itemImportarXML);
        barra.add(menu);

        frame.setJMenuBar(barra);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object botonPulsado = e.getSource();
        if (botonPulsado == altaJugadorButton){
            altaJugador();
        } else if (botonPulsado == bajaJugadorButton){
            //bajaJugador();
        } else if (botonPulsado == mostrarDatosButton){
            mostrarDatosLinkedList();
        } else if (botonPulsado == itemExportarXML){
            JFileChooser selectorArchivo = new JFileChooser();
            int opcionSeleccionada = selectorArchivo.showSaveDialog(null);
            if (opcionSeleccionada == JFileChooser.APPROVE_OPTION) {
                File fichero = selectorArchivo.getSelectedFile();
                exportarXML(fichero);
            }
        } else if (botonPulsado == itemImportarXML){
            JFileChooser selectorArchivo = new JFileChooser();
            int opcion = selectorArchivo.showOpenDialog(null);
            if (opcion == JFileChooser.APPROVE_OPTION) {
                File fichero = selectorArchivo.getSelectedFile();
                importarXML(fichero);
                refrescarComboBox();
            }
        }


    }

    private void importarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document documento = builder.parse(fichero);

            //Recorro cada uno de los nodos coche para obtener sus campos
            NodeList jugadores = documento.getElementsByTagName("jugador");
            for (int i = 0; i < jugadores.getLength(); i++) {
                Node jugador = jugadores.item(i);
                Element elemento = (Element) jugador;


                String nombre = elemento.getElementsByTagName("nombre").item(0).getChildNodes().item(0).
                        getNodeValue();
                double peso = Double.parseDouble(elemento.getElementsByTagName("peso").item(0).getChildNodes().
                        item(0).getNodeValue());
                int edad = Integer.parseInt(elemento.getElementsByTagName("edad").item(0).getChildNodes().
                        item(0).getNodeValue());
                String posicion = elemento.getElementsByTagName("posicion").item(0).getChildNodes().item(0).
                        getNodeValue();
                altaJugador();
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    private void exportarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;

        try {
            builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();

            //Creo documento que representa arbol XML
            Document documento = dom.createDocument(null, "xml", null);

            //Creo el nodo raiz (coches) y lo añado al documento
            Element raiz = documento.createElement("coches");
            documento.getDocumentElement().appendChild(raiz);

            Element nodoJugador;
            Element nodoDatos;
            Text dato;

            //Por cada coche de la lista, creo un nodo coche
            for (Jugador jugador : lista) {

                //Creo un nodo coche y lo añado al nodo raiz (coches)
                nodoJugador = documento.createElement("jugador");
                raiz.appendChild(nodoJugador);

                //A cada nodo coche le añado los nodos marca y modelo
                nodoDatos = documento.createElement("nombre");
                nodoJugador.appendChild(nodoDatos);

                //A cada nodo de datos le añado el dato
                dato = documento.createTextNode(jugador.getNombre());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("posicion");
                nodoJugador.appendChild(nodoDatos);

                dato = documento.createTextNode(jugador.getPosicion());
                nodoDatos.appendChild(dato);
            }

            //Transformo el documento anterior en un ficho de texto plano
            Source src = new DOMSource(documento);
            Result result = new StreamResult(fichero);

            Transformer transformer = null;
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(src, result);

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }
}
