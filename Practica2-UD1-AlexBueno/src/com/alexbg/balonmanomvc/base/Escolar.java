package com.alexbg.balonmanomvc.base;
/***
 *
 */

import java.time.LocalDate;

/***
 *
 */
public class Escolar extends Jugador{
    /***
     *
     */
    public String categoria;

    /***
     *
     */
    public Escolar(){
        super();
        this.categoria = "";
    }

    /***
     *
     * @param nombreJugador
     * @param club
     * @param posicion
     * @param peso
     * @param altura
     * @param fechaAlta
     * @param categoria
     */
    public Escolar(String nombreJugador, String club, String posicion, double peso, double altura, LocalDate fechaAlta, String categoria) {
        super(nombreJugador, club, posicion, peso, altura, fechaAlta);
        this.categoria = categoria;
    }

    /***
     *
     * @return categoria
     */
    public String getCategoria() {
        return categoria;
    }

    /***
     *
     * @param categoria
     */
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    /***
     *
     * @return
     */
    @Override
    public String toString() {
        return "Escolar{" +
                "nombreJugador='" + nombreJugador + '\'' +
                ", club='" + club + '\'' +
                ", posicion='" + posicion + '\'' +
                ", peso='" + peso + '\'' +
                ", altura=" + altura +
                ", fechaAlta=" + fechaAlta +
                ", categoria=" + categoria +
                '}';
    }
}
