package com.alexbg.balonmanomvc.gui;

import com.alexbg.balonmanomvc.base.Escolar;
import com.alexbg.balonmanomvc.base.Federado;
import com.alexbg.balonmanomvc.base.Jugador;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class JugadorModelo {
    public ArrayList<Jugador> listaJugadores;

    public JugadorModelo(){
        listaJugadores = new ArrayList<Jugador>();
    }

    public ArrayList<Jugador> obtenerJugadores(){
        return listaJugadores;
    }

    /***
     *
     * @param nombreJugador
     * @param club
     * @param posicion
     * @param peso
     * @param altura
     * @param fechaAlta
     * @param torneo
     */
    public void altaFederado(String nombreJugador, String club, String posicion, double peso, double altura, LocalDate fechaAlta, String torneo){
        Federado nuevoFederado = new Federado(nombreJugador, club, posicion, peso, altura, fechaAlta, torneo);
            listaJugadores.add(nuevoFederado);
    }

    public void altaEscolar(String nombreJugador, String club, String posicion, double peso, double altura, LocalDate fechaAlta, String categoria){
        Escolar nuevoEscolar = new Escolar(nombreJugador, club, posicion, peso, altura, fechaAlta, categoria);

        listaJugadores.add(nuevoEscolar);
    }

    /***
     * metodo que sirve para que salte un error si se intenta añadir un jugador que ya existe
     * @param nombreJugador
     * @return
     */
    public boolean existeNombreJugador(String nombreJugador){
        for (Jugador unJugador: listaJugadores) {
            if (unJugador.getNombreJugador().equals(nombreJugador)){
                return true;
            }
        }
        return false;
    }

    public boolean existePosicion(String posicion){
        for (Jugador unJugador: listaJugadores){
            if (unJugador.getPosicion().equals(posicion)){
                return true;
            }
        }
        return false;
    }

    /***
     * metodo usado para poder exportar el archivo creado como un fichero con extension xml a nuestro ordenador
     * @param fichero
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        //Añado el nodo raiz - la primera etiqueta que contiene a las demas
        Element raiz = documento.createElement("Vehiculos");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoJugador = null, nodoDatos = null;
        Text texto = null;

        for (Jugador unJugador: listaJugadores) {
            if (unJugador instanceof Federado){
                nodoJugador = documento.createElement("Federado");
            } else {
                nodoJugador = documento.createElement("Escolar");
            }
            raiz.appendChild(nodoJugador);

            nodoDatos = documento.createElement("Nombre Jugador");
            nodoJugador.appendChild(nodoDatos);

            nodoDatos = documento.createElement("Peso");
            nodoJugador.appendChild(nodoDatos);

            nodoDatos = documento.createElement("Altura");
            nodoJugador.appendChild(nodoDatos);

            nodoDatos = documento.createElement("Posicion");
            nodoJugador.appendChild(nodoDatos);

            nodoDatos = documento.createElement("Club");
            nodoJugador.appendChild(nodoDatos);

            nodoDatos = documento.createElement("Fecha Alta");
            nodoJugador.appendChild(nodoDatos);

            nodoDatos = documento.createElement("Categoria");
            nodoJugador.appendChild(nodoDatos);

            nodoDatos = documento.createElement("Torneo");
            nodoJugador.appendChild(nodoDatos);

            texto = documento.createTextNode(unJugador.getFechaAlta().toString());
            nodoDatos.appendChild(texto);

            if (unJugador instanceof Federado){
                nodoDatos = documento.createElement("torneo");
                nodoJugador.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((Federado) unJugador).getTorneo()));
                nodoDatos.appendChild(texto);
            } else {
                nodoDatos = documento.createElement("categoria");
                nodoJugador.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((Escolar) unJugador).getCategoria()));
                nodoDatos.appendChild(texto);
            }
        }
        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(fichero);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, resultado);
    }

    /***
     * metodo usado a la hora de querer trabajar con un xml ya creado pero que no esta rellenado
     * @param fichero
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {
        listaJugadores = new ArrayList<Jugador>();
        Federado nuevoFederado = null;
        Escolar nuevoEscolar = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i < listaElementos.getLength(); i++) {
            Element nodoJugador = (Element) listaElementos.item(i);

            if (nodoJugador.getTagName().equals("Federado")) {
                nuevoFederado = new Federado();
                nuevoFederado.setNombreJugador(nodoJugador.getChildNodes().item(0).getTextContent());
                nuevoFederado.setClub(nodoJugador.getChildNodes().item(1).getTextContent());
                nuevoFederado.setPeso(Double.parseDouble(nodoJugador.getChildNodes().item(2).getTextContent()));
                nuevoFederado.setAltura(Double.parseDouble(nodoJugador.getChildNodes().item(3).getTextContent()));
                nuevoFederado.setFechaAlta(LocalDate.parse(nodoJugador.getChildNodes().item(4).getTextContent()));
                nuevoFederado.setTorneo(nodoJugador.getChildNodes().item(5).getTextContent());

                listaJugadores.add(nuevoFederado);

            } else if (nodoJugador.getTagName().equals("Escolar")){
                nuevoEscolar = new Escolar();
                nuevoEscolar.setNombreJugador(nodoJugador.getChildNodes().item(0).getTextContent());
                nuevoEscolar.setClub(nodoJugador.getChildNodes().item(1).getTextContent());
                nuevoEscolar.setPeso(Double.parseDouble(nodoJugador.getChildNodes().item(2).getTextContent()));
                nuevoEscolar.setAltura(Double.parseDouble(nodoJugador.getChildNodes().item(3).getTextContent()));
                nuevoEscolar.setFechaAlta(LocalDate.parse(nodoJugador.getChildNodes().item(4).getTextContent()));
                nuevoEscolar.setCategoria(nodoJugador.getChildNodes().item(5).getTextContent());

                listaJugadores.add(nuevoEscolar);
            }
        }
    }
}
