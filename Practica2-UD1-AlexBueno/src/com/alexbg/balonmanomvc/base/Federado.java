package com.alexbg.balonmanomvc.base;

import java.time.LocalDate;

public class Federado extends Jugador{
    public String torneo;

    public Federado(){
        super();
        this.torneo = "";
    }

    /***
     *
     * @param nombreJugador
     * @param club
     * @param posicion
     * @param peso
     * @param altura
     * @param fechaAlta
     * @param torneo
     */
    public Federado(String nombreJugador, String club, String posicion, double peso, double altura, LocalDate fechaAlta, String torneo) {
        super(nombreJugador, club, posicion, peso, altura, fechaAlta);
        this.torneo = torneo;
    }

    /***
     *
     * @return
     */
    public String getTorneo() {
        return torneo;
    }

    /**
     *
     * @param torneo
     */
    public void setTorneo(String torneo) {
        this.torneo = torneo;
    }

    /***
     *
     * @return
     */
    @Override
    public String toString() {
        return "Federado{" +
                "nombreJugador='" + nombreJugador + '\'' +
                ", club='" + club + '\'' +
                ", posicion='" + posicion + '\'' +
                ", peso='" + peso + '\'' +
                ", altura=" + altura +
                ", fechaAlta=" + fechaAlta +
                ", torneo=" + torneo +
                '}';
    }
}
