package com.alexbg.balonmanomvc.gui;

import com.alexbg.balonmanomvc.base.Escolar;
import com.alexbg.balonmanomvc.base.Jugador;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

public class Ventana {
    public JPanel panel1;
    public JFrame frame;

    public JLabel equipoTXT;
    public JLabel nombreTXT;
    public JLabel pesoTXT;
    public JLabel alturaTXT;
    public JLabel posicionCombobox;
    public JLabel clubTXT;
    public JLabel fechaAltaTXT;
    public JLabel divisionTXT;
    public JLabel categoriaTXT;
    public JLabel torneoTXT;
    public JLabel EscolarFederado;

    public JRadioButton escolarRadioButton;
    public JRadioButton federadoRadioButton;
    public JRadioButton segundaRadioButton1;
    public JRadioButton primeraRadioButton;
    public JRadioButton territorialRadioButton1;

    public JTextField nombreJTXT;
    public JTextField pesoJTXT;
    public JTextField alturaJTXT;
    public JTextField torneoJTXT;
    public JTextField clubJTXT;

    public JComboBox posicionCombo;
    public JComboBox categoria;
    public JComboBox informacion;

    public JButton nuevoButton;
    public JButton exportarButton;
    public JButton importarButton;

    public JList list1;

    public DatePicker fechaAltaDPicker;



    public DefaultComboBoxModel<Jugador> dcbmposi;
    public DefaultComboBoxModel<Escolar> dcbmcategoria;
    public DefaultComboBoxModel<Ventana> dcbminfo;
    public DefaultListModel<Jugador> dlmJugador;

    public Ventana(){
        frame = new JFrame("Jugador MVC");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        initComponents();
        aniadirDatosComboBox();
    }

    private void initComponents(){
        dlmJugador = new DefaultListModel<Jugador>();
        dcbmposi = new DefaultComboBoxModel<Jugador>();
        dcbmcategoria = new DefaultComboBoxModel<Escolar>();
        posicionCombo.setModel(dcbmposi);
        categoria.setModel(dcbmcategoria);
        list1.setModel(dlmJugador);


    }

    private void aniadirDatosComboBox(){
        posicionCombo.addItem("POSICION");
        posicionCombo.addItem("Portero");
        posicionCombo.addItem("Pivote");
        posicionCombo.addItem("Central");
        posicionCombo.addItem("Lateral");
        posicionCombo.addItem("Extremo");

        categoria.addItem("CATEGORIA");
        categoria.addItem("BENJAMIN");
        categoria.addItem("ALEVIN");
        categoria.addItem("INFANTIL");
        categoria.addItem("CADETE");
        categoria.addItem("JUVENIL");

    }
}
