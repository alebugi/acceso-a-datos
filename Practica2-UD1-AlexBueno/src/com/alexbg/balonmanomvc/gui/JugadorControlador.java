package com.alexbg.balonmanomvc.gui;

import com.alexbg.balonmanomvc.base.Escolar;
import com.alexbg.balonmanomvc.base.Federado;
import com.alexbg.balonmanomvc.base.Jugador;
import com.alexbg.balonmanomvc.util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.util.Properties;

public class JugadorControlador implements ActionListener, ListSelectionListener, WindowListener{
    private Ventana vista;
    private JugadorModelo modelo;
    private File ultimaRutaExportada;

    /***
     * constructor rellenado con datos que pasaremos por parametros de entrada
     * @param vista
     * @param modelo
     */
    public JugadorControlador(Ventana vista, JugadorModelo modelo){
        this.vista = vista;
        this.modelo = modelo;

        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            e.printStackTrace();
        }

        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);
    }

    /***
     * metodo el cual sirve para dar eventos a los botones de la aplicacion
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();
        switch (actionCommand){
            case "Nuevo":
                if (hayCamposVacios()) {
                    Util.mensajeError("Los siguientes campos no pueden estar vacios\n" +
                            "Matricula\nMarca\nModelo\nFecha");
                    break;
                }

                if (modelo.existeNombreJugador(vista.nombreTXT.getText())){
                    Util.mensajeError("Ya existe un jugador con ese nombre\n+"+
                            vista.nombreTXT.getText());
                    break;
                }

                if (vista.federadoRadioButton.isSelected()){
                    modelo.altaFederado(vista.nombreJTXT.getText(), vista.clubJTXT.getText(), vista.posicionCombobox.getText(), Double.parseDouble(vista.pesoJTXT.getText()),
                            Double.parseDouble(vista.alturaJTXT.getText()), vista.fechaAltaDPicker.getDate(), vista.torneoJTXT.getText());
                } else {
                    modelo.altaEscolar(vista.nombreJTXT.getText(), vista.clubJTXT.getText(), vista.posicionCombobox.getText(), Double.parseDouble(vista.pesoJTXT.getText()),
                            Double.parseDouble(vista.alturaJTXT.getText()), vista.fechaAltaDPicker.getDate(), vista.categoriaTXT.getText());
                }
                limpiarCampos();
                refrescar();
                break;
            case "Importar":
                JFileChooser selectorFichero = Util.crearSelectorFicheros(ultimaRutaExportada,
                        "Archivo XML", "xml");
                int opt = selectorFichero.showOpenDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    }
                    refrescar();
                }
                break;
            case "Exportar":
                JFileChooser selectorFichero2 = Util.crearSelectorFicheros(ultimaRutaExportada,
                        "Archivos XML", "xml");
                int opt2 = selectorFichero2.showSaveDialog(null);
                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            case "Escolar":
                vista.EscolarFederado.setText("Categoria");
                break;
            case "Federado":
                vista.EscolarFederado.setText("Torneo");
                break;
        }
    }

    /***
     * este metodo es usado para que te avise de si te has olvidado de rellenar un campo
     * @return
     */
    private boolean hayCamposVacios(){
        if (vista.nombreTXT.getText().isEmpty() ||
                vista.clubTXT.getText().isEmpty() ||
                vista.pesoTXT.getText().isEmpty() ||
                vista.alturaTXT.getText().isEmpty() ||
                vista.fechaAltaTXT.getText().isEmpty() ||
                vista.torneoTXT.getText().isEmpty() ||
                vista.categoriaTXT.getText().isEmpty()){
            return true;
        }
        return false;
    }

    /***
     * metodo que se usa para una vez rellenado y añadido los datos, se borren y permita añadir unos datos nuevos
     */
    private void limpiarCampos(){
        vista.nombreTXT.requestFocus();
        vista.clubTXT.setText(null);
        vista.pesoTXT.setText(null);
        vista.alturaTXT.setText(null);
        vista.fechaAltaTXT.setText(null);
        vista.torneoTXT.setText(null);
        vista.categoriaTXT.setText(null);
    }

    /***
     * metodo para limpiar la memoria del teclado
     */
    private void refrescar() {
        vista.dlmJugador.clear();
        for (Jugador unJugador : modelo.obtenerJugadores()) {
            vista.dlmJugador.addElement(unJugador);
        }
    }

    /***
     * metodo que sirve para dar eventos a los diferentes botones y radioButtones
     * @param listener
     */
    private void addActionListener(ActionListener listener) {
        vista.escolarRadioButton.addActionListener(listener);
        vista.federadoRadioButton.addActionListener(listener);
        vista.primeraRadioButton.addActionListener(listener);
        vista.segundaRadioButton1.addActionListener(listener);
        vista.territorialRadioButton1.addActionListener(listener);
        vista.exportarButton.addActionListener(listener);
        vista.importarButton.addActionListener(listener);
        vista.nuevoButton.addActionListener(listener);
    }

    private void addWindowListener(WindowListener listener){
        vista.frame.addWindowListener(listener);
    }

    private void addListSelectionListener(ListSelectionListener listener) {
        vista.list1.addListSelectionListener(listener);
    }

    /***
     * este metodo es usado para una vez añadidos los datos cargarlos en un archivo .conf
     * @throws IOException
     */
    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("jugadores.conf"));
        ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));
    }

    private void actualizarDatosConfiguracion(File ultimaRutaExportada) {
        this.ultimaRutaExportada = ultimaRutaExportada;
    }

    private void guardarConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("jugadores.conf"),
                "Datos configuracion jugadores");

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()){
            Jugador jugadorseleccionado = (Jugador)vista.list1.getSelectedValue();
            vista.nombreJTXT.setText((jugadorseleccionado.getNombreJugador()));
            vista.pesoJTXT.setText(String.valueOf(jugadorseleccionado.getPeso()));
            vista.alturaJTXT.setText(String.valueOf(jugadorseleccionado.getAltura()));
            vista.posicionCombobox.setText((jugadorseleccionado.getPosicion()));
            vista.clubJTXT.setText((jugadorseleccionado.getClub()));
            vista.fechaAltaDPicker.setDate(jugadorseleccionado.getFechaAlta());

            if (jugadorseleccionado instanceof Federado){
                vista.federadoRadioButton.doClick();
                vista.torneoJTXT.setText(String.valueOf(
                        ((Federado) jugadorseleccionado).getTorneo()));
            } else {
                vista.escolarRadioButton.doClick();
                vista.categoriaTXT.setText(String.valueOf(((Escolar) jugadorseleccionado).getCategoria()));
            }
        }

    }

    @Override
    public void windowClosing(WindowEvent e) {
        int resp = Util.mensajeConfirmacion("¿Desea salir de la ventana?", "Salir");
        if (resp == JOptionPane.OK_OPTION) {
            try {
                guardarConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }
}
