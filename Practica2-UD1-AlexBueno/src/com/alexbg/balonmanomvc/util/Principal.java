package com.alexbg.balonmanomvc.util;

import com.alexbg.balonmanomvc.gui.JugadorControlador;
import com.alexbg.balonmanomvc.gui.JugadorModelo;
import com.alexbg.balonmanomvc.gui.Ventana;

public class Principal {
    /***
     * lugar donde instanciamos todas las clases para poder ejecutar el programa
     * @param args
     */
    public static void main(String[] args) {
        Ventana vista = new Ventana();
        JugadorModelo modelo = new JugadorModelo();
        JugadorControlador controlador = new JugadorControlador(vista, modelo);
    }

}
