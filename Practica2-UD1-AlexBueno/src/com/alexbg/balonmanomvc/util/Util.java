package com.alexbg.balonmanomvc.util;


import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

/**
 * Created by DAM on 18/11/2020.
 */
public class Util {
    /***
     * que nos avise de si hay algun problema a la hora de rellenar los datos o pulsar algun boton
     * @param mensaje
     */
    public static void mensajeError(String mensaje) {
        JOptionPane.showMessageDialog(null,mensaje,"Error",JOptionPane.ERROR_MESSAGE);
    }

    /***
     * ventana emergente una vez se quiera cerrar el programa si ya hemos terminado
     * @param mensaje
     * @param titulo
     * @return
     */
    public static int mensajeConfirmacion(String mensaje, String titulo) {
        return JOptionPane.showConfirmDialog(null,mensaje,titulo,JOptionPane.YES_NO_OPTION);
    }

    /***
     * metodo que sirve para crear el archivo si no existiese de primeras al iniciar el programa
     * @param rutaDefecto
     * @param tipoArchivos
     * @param extension
     * @return
     */
    public static JFileChooser crearSelectorFicheros(File rutaDefecto, String tipoArchivos, String extension) {
        JFileChooser selectorFichero = new JFileChooser();
        if (rutaDefecto!=null) {
            selectorFichero.setCurrentDirectory(rutaDefecto);
        }
        if (extension!=null) {
            FileNameExtensionFilter filtro = new FileNameExtensionFilter(tipoArchivos,extension);
            selectorFichero.setFileFilter(filtro);
        }
        return selectorFichero;
    }
}
