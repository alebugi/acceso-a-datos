package com.alexbg.balonmanomvc.base;

import java.time.LocalDate;

/***
 *
 */
public abstract class Jugador {
    /***
     *
     */
    public String nombreJugador;
    public String club;
    public String posicion;
    public double peso;
    public double altura;
    public LocalDate fechaAlta;

    /***
     *
     * @param nombreJugador
     * @param club
     * @param posicion
     * @param peso
     * @param altura
     * @param fechaAlta
     */
    public Jugador(String nombreJugador, String club, String posicion, double peso, double altura, LocalDate fechaAlta){
        this.nombreJugador = nombreJugador;
        this.club = club;
        this.posicion = posicion;
        this.peso = peso;
        this.altura = altura;
        this.fechaAlta = fechaAlta;
    }

    /***
     *
     */
    public Jugador(){
        this.nombreJugador = "";
        this.club = "";
        this.posicion = "";
        this.peso = 0;
        this.altura = 0;
        this.fechaAlta = LocalDate.ofEpochDay(0);
    }

    /***
     *
     * @return nombre
     */
    public String getNombreJugador() {
        return nombreJugador;
    }

    /***
     *
     * @param nombreJugador
     */
    public void setNombreJugador(String nombreJugador) {
        this.nombreJugador = nombreJugador;
    }

    public String getClub() {
        return club;
    }

    /***
     *
     * @param club
     */
    public void setClub(String club) {
        this.club = club;
    }

    public String getPosicion() {
        return posicion;
    }

    /***
     *
     * @param posicion
     */
    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    public Double getPeso() {
        return peso;
    }

    /***
     *
     * @param peso
     */
    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getAltura() {
        return altura;
    }

    /***
     *
     * @param altura
     */
    public void setAltura(double altura) {
        this.altura = altura;
    }

    public LocalDate getFechaAlta() {
        return fechaAlta;
    }

    /***
     *
     * @param fechaAlta
     */
    public void setFechaAlta(LocalDate fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    @Override
    public String toString() {
        return "Jugador{" +
                "nombreJugador='" + nombreJugador + '\'' +
                ", peso=" + peso +
                ", altura=" + altura +
                ", fechaAlta=" + fechaAlta +
                '}';
    }
}
